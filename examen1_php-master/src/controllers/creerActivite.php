<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
require_once __DIR__ . "/../models/Activite.php";

/**
 * Class creerActivite
 */
class creerActivite
{

    /** Fonction getActivites qui créer un nouveau tableau qui contiendra les activités et l'entrepôse dans la
     * variable globale $_SESSION
     * @return array
     */
    private static function getActivites(): array
    {

        if (!isset($_SESSION["activites"])) {
            $_SESSION["activites"] = [];
        }
        return $_SESSION["activites"];
    }

    /*TODO Compléter la méthode de validation
    Critères de validation :
    - Accepter lettres, ",", ";" "-" "'" pour les champs texte */
    /** Fonction estValide qui reçoit en paramètre une chaîne de caractère de type string
     * La fonction utilsie pregmatch pour valider que la chaîne entrée en paramètre respect bien la formule.
     * Elle retourne ensuite de cela true si elle est valide.
     * @param string $chaine
     * @return bool
     */
    private static function estValide(string $chaine): bool
    {
       if(preg_match("/^[A-Z]|[a-z]|[,]|[;]|[\-]|[']$/", $chaine) == true){
           return true;
       }

    }


    /** Fonction ajoutNouvelleActivite qui reçoit en paramètre une variable de type array et qui retourne une variable de type bool.
     * La fonction vérifie les données de l'activité avec la méthode estvalide et si c'est valide, elle ajoute l'objet Activite créer plus haut
     * au tableau des activités.
     * @param array $details
     * @return bool
     */

    public static function ajoutNouvelleActivite(array $details): bool
    {
        $creation = false;

        //@TODO Ajouter les instructions pour valider les données de l'activité ($details) à l'aide de la méthode estValide
        $activite = new Activite($details);
        if(self::estValide($activite->getActivite()) == true && self::estValide($activite->getDeterminant()) == true && self::estValide($activite->getFacteurMotivation()) == true&& self::estValide($activite->getLieu()) == true && self::estValide($activite->getPartenaire()) == true)
        {

            $creation = self::getActivites()->ajouterActivite($activite);


        }
        //@TODO Si valide, l'ajouter au tableau des activités

        //Retourner vrai si l'activité a été ajoutée, faux sinon
        return $creation;
    }
}