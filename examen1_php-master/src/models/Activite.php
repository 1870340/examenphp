<?php


/**
 * Class Activite
 */
class Activite
{
    /**
     * On créer les propriétés de la classe
     */
    private string $lieu;
    private string $partenaire;
    private string $activite;
    private string $determinant;
    private string $facteur_motivation;
    private array $tab_activite;

    /**
     * On créer aussi le contructeur de la classe
     * @param array $activiteDetails
     */
    public function __construct(array $activiteDetails)
    {
        $this->lieu = isset( $activiteDetails["lieu"]) ? $activiteDetails["lieu"] : "teste";
        $this->partenaire = isset( $activiteDetails["partenaire"]) ? $activiteDetails["partenaire"] : "teste";
        $this->activite = isset( $activiteDetails["activite"]) ? $activiteDetails["activite"] : "teste";
        $this->determinant = isset( $activiteDetails["determinant"]) ? $activiteDetails["determinant"] : "teste";
        $this->facteur_motivation = isset( $activiteDetails["facteur-motivation"]) ? $activiteDetails["facteur-motivation"] : "teste";
        $this->tab_activite = [];
    }

    /** La fonction getLieu qui retourne le lieu
     * @return string
     */
    public function getLieu(): string
    {
        return $this->lieu;
    }

    /** Fonction setLieu qui instancie la variable lieu
     * @param string $lieu
     */
    public function setLieu(string $lieu): void
    {
        $this->lieu = $lieu;
    }

    /**La fonction getpartenaire qui retourne le partenaire
     * @return string
     */
    public function getPartenaire(): string
    {
        return $this->partenaire;
    }

    /**Fonction setPartenaire qui instancie la variable partenaire
     * @param string $partenaire
     */
    public function setPartenaire(string $partenaire): void
    {
        $this->partenaire = $partenaire;
    }

    /**La fonction getActivite qui retourne l'activite
     * @return string
     */
    public function getActivite(): string
    {
        return $this->activite;
    }

    /**Fonction setActivite qui instancie la variable activite
     * @param string $activite
     */
    public function setActivite(string $activite): void
    {
        $this->activite = $activite;
    }

    /**La fonction getDeterminant qui retourne le determinant
     * @return string
     */
    public function getDeterminant(): string
    {
        return $this->determinant;
    }

    /**Fonction setDeterminant qui instancie la variable Determinant
     * @param string $determinant
     */
    public function setDeterminant(string $determinant): void
    {
        $this->determinant = $determinant;
    }

    /**La fonction getFacteurMotivation qui retourne la motivation
     * @return string
     */
    public function getFacteurMotivation(): string
    {
        return $this->facteur_motivation;
    }

    /**Fonction setFacteurMotivation qui instancie la variable facteur_motivation
     * @param string $facteur_motivation
     */
    public function setFacteurMotivation(string $facteur_motivation): void
    {
        $this->facteur_motivation = $facteur_motivation;
    }

    /** Fonction getactivites qui retourne le tableau activite
     * @return array
     */
    public function getactivites():array
    {
        return $this->tab_activite;
    }

    /**Fonction setJoueurs qui reçoit en paramètre une variable de type array et instancie la variable tab-activite
     * @param array $activite
     */
    public function setJoueurs(array $activite): void
    {
        $this->tab_activite = $activite;
    }

    /**Fonction ajouterActivite qui ajoute un objet de type activite à la propriété tab_activite
     * @param Activite $activite
     * @return bool
     */
    public function ajouterActivite(Activite $activite):bool
    {
        array_push( $this->tab_activite, $activite);
        return true;

    }


    /** retourne le nom de l'activite
     * @return string
     */
    function __toString()
    {
        return $this->getActivite();
    }




}