<?php
require_once __DIR__ . "/../../src/controllers/creerActivite.php";

use PHPUnit\Framework\TestCase;

class creerActiviteTest extends TestCase
{
    static function setUpBeforeClass(): void
    {
        $_SESSION = array();
    }
    //@TODO implémenter ce test
    public function testEstValideChaineValide():void
    {
        $array = ["nom" => "Nathalie",
                "nom2" => "Jean-marc",
                "nom3" => "'tite laine'"];

        $this->assertEquals(true,creerActivite::ajoutNouvelleActivite($array));
    }

    //@TODO implémenter ce test
    public function testEstValideChaineInValide():void
    {
        $array = ["nom" => "33 Cité-des-jeunes"];

        $this->assertEquals(false,creerActivite::ajoutNouvelleActivite($array) );
    }
    public function tearDown(): void
    {
        $_SESSION = array();
    }
}
